a = 1
b = 0
c = 1
x = 0

def gauss(a,b,c,x):
    """this functions accepts 4 parameters
    a,b,c, and x as the independent variable."""
    f = a*np.exp(-((x-b)**2)/(2*(c**2)))
    return f
gauss(1,0,1,1)


gaus = open("gaus_int.out","w")
gaus.write("The value is around 0.607")
gaus.write("\n")
gaus.write("The value is around the value of pi when you square the integral")
gaus.write("\n")
gaus.write("3.14")
gaus.write('\n')
gaus.write("The precieison is better than 1e-03 for n >= 6")
gaus.write('\n')
gaus.write("Interval Boundary >= 2.7 for greater than 99% of the square of the indefinite integral included")
gaus.write('\n')
gaus.close()



print("The output when x = 1 is ", gauss(1,0,1,1))


plt.xlim(-3,5)
x = np.linspace(-3,5,100)
a = plt.plot(x, gauss(1,0,1,x), 'bo', label = 'default gauss')
b = plt.plot(x, gauss(0.5,0,1,x), 'h', label = 'gauss with a = 0.5')
#changing a changes the height of the function
c = plt.plot(x, gauss(1,1,1,x), '-c',  label = 'gauss with b = 1')
#changing b shifts the function to the right if greater than 0 and to the left if less than 0
d = plt.plot(x, gauss(1,0,0.5,x), '-o', label = 'gauss with c = 0.5')
#changing c elongates or shrinks the width of the function
plt.legend()
plt.ylabel("some numbers")
plt.xlabel("x values")
plt.show()



