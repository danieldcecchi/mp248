def sym_gauss_int_sqr(g, a, b, n):
    """this function compute the integral of a specified funciton g using the trapezoidal rule"""
    h = float(b-a)/n
    result = 0.5*g(a) + 0.5*g(b)
    for i in range(1, n):
        result += g(a + i*h)
    result *= h
    return result
def gaussian(x):
    function = np.exp(-((x-0)**2))
    return function
xb = float(input("What are the bounds you would like? "))
a = xb*(-1)

b = xb
n = int(input("How many grids would you like? "))
if n is 0:
    n = 10
    

answer = sym_gauss_int_sqr(gaussian,a,b,n)

print(answer**2)
percent_precision = ((answer)/1.77241)*100
print(100 - percent_precision)
pi_percent_precision = ((answer**2)/pi)*100
print(100-pi_percent_precision)